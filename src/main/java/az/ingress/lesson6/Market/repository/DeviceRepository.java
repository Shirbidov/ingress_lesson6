package az.ingress.lesson6.Market.repository;

import az.ingress.lesson6.Market.domain.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<DeviceEntity, Long> {
}
