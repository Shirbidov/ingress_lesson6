package az.ingress.lesson6.Market.service;

import az.ingress.lesson6.Market.dto.DeviceRequestDTO;
import az.ingress.lesson6.Market.dto.DeviceResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DeviceService {
    DeviceResponseDTO createDevice(DeviceRequestDTO requestDTO);

    DeviceResponseDTO updateDevice(Long id,DeviceRequestDTO requestDTO);


    DeviceResponseDTO getDeviceByID(Long id);

    void deleteDevice(Long id);


    Page<DeviceResponseDTO> getDeviceList(Pageable pageable);
}
