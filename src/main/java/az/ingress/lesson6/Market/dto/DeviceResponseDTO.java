package az.ingress.lesson6.Market.dto;


import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceResponseDTO {
    private Long id;
    private String name;
    private String serialNumber;
}
