package az.ingress.lesson6.Market.rest;

import az.ingress.lesson6.Market.domain.DeviceEntity;
import az.ingress.lesson6.Market.dto.DeviceRequestDTO;
import az.ingress.lesson6.Market.dto.DeviceResponseDTO;
import az.ingress.lesson6.Market.service.DeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/devices")
public class DeviceController {
    private final DeviceService deviceService;
    @PostMapping
    public DeviceResponseDTO createDevice(@RequestBody @Validated DeviceRequestDTO dto) {
        return deviceService.createDevice(dto);
    }

    @PutMapping("/{id}")
    public DeviceResponseDTO updateDevice(@PathVariable Long id,
                                          @RequestBody DeviceRequestDTO requestDTO){
        return deviceService.updateDevice(id,requestDTO);
    }

    @GetMapping("/{id}")
    public DeviceResponseDTO getDeviceByID(@PathVariable Long id){
        return deviceService.getDeviceByID(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
         deviceService.deleteDevice(id);
    }
    @GetMapping
    public Page<DeviceResponseDTO> getDeviceList(Pageable pageable){
        return deviceService.getDeviceList(pageable);
    }


}
