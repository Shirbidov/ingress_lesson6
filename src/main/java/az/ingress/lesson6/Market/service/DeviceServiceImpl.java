package az.ingress.lesson6.Market.service;

import az.ingress.lesson6.Market.domain.DeviceEntity;
import az.ingress.lesson6.Market.dto.DeviceRequestDTO;
import az.ingress.lesson6.Market.dto.DeviceResponseDTO;
import az.ingress.lesson6.Market.exaption.NotFoundException;
import az.ingress.lesson6.Market.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@RequiredArgsConstructor
public class DeviceServiceImpl implements DeviceService {

    final private DeviceRepository deviceRepository;

    @Override
    public DeviceResponseDTO createDevice(DeviceRequestDTO requestDTO) {
        DeviceEntity device =
                DeviceEntity.
                        builder()
                        .name(requestDTO.getName())
                        .serialNumber(requestDTO.getSerialNumber())
                        .build();
        final DeviceEntity deviceEntitySaved = deviceRepository.save(device);
        return DeviceResponseDTO
                .builder()
                .id(deviceEntitySaved.getId())
                .name(deviceEntitySaved.getName())
                .serialNumber(deviceEntitySaved.getSerialNumber())
                .build();
    }

    @Override
    public DeviceResponseDTO updateDevice(Long id, DeviceRequestDTO requestDTO) {
        DeviceEntity device =
                DeviceEntity.
                        builder()
                        .id(id)
                        .name(requestDTO.getName())
                        .serialNumber(requestDTO.getSerialNumber())
                        .build();
        final DeviceEntity deviceEntitySaved = deviceRepository.save(device);
        return DeviceResponseDTO
                .builder()
                .id(deviceEntitySaved.getId())
                .name(deviceEntitySaved.getName())
                .serialNumber(deviceEntitySaved.getSerialNumber())
                .build();
    }

    @Override
    public DeviceResponseDTO getDeviceByID(Long id) {
        final DeviceEntity deviceEntity = deviceRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return DeviceResponseDTO
                .builder()
                .id(deviceEntity.getId())
                .name(deviceEntity.getName())
                .serialNumber(deviceEntity.getSerialNumber())
                .build();
    }

    @Override
    public void deleteDevice(Long id) {
        deviceRepository.deleteById(id);
    }

    @Override
    public Page<DeviceResponseDTO> getDeviceList(Pageable pageable) {
        return deviceRepository.findAll(pageable)
                .map(deviceEntity -> DeviceResponseDTO
                        .builder()
                        .id(deviceEntity.getId())
                        .name(deviceEntity.getName())
                        .serialNumber(deviceEntity.getSerialNumber())
                        .build());
    }

}
